import React from 'react';

const renderListItems = (listOfItems, Component) => {
	return listOfItems.map(itemInList => <Component key={itemInList.id} text={itemInList.text} />)
}

export default renderListItems;