import React from 'react';

const ListItem = props => <li key={props.id}>{props.text}</li>

export default ListItem;