import React from 'react';

const List = props => <ul className={props.classToRender}>{props.render()}</ul>;

export default List;