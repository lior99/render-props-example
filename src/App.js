import React, { Component } from 'react';
import logo from './logo.svg';
import List from './components/List';
import ListItem from './components/ListItem';
import './App.css';
import renderListItems from './components/utils';

class App extends Component {
	state = {
		items: [
			{ id: 1, text: 'this is item number 1'},
			{ id: 2, text: 'hello there'},
			{ id: 3, text: 'settings'},
			{ id: 4, text: 'something else'},
		]
	}	

	render() {
		const { items } = this.state;

		const renderFunc = () => renderListItems(items, ListItem);

		return (
			<div className="App">
				<List render={renderFunc} classToRender="some-list-class" />
			</div>
		);
	}
}

export default App;
