# Example

## Render props in react example

This is an example of rendering a simple list of items using the render as a prop concept

We have a List component who gets a render function passed to it, which is responsible for rendering the list itself. The List component calls the function, it can be what ever function we pass to it :)

The List component looks like :
```html
const ListItem = props => <li key={props.id}>{props.text}</li>
```
The render function is an outside function from a file called utils: 
```html
const renderListItems = (listOfItems) => {
	return listOfItems.map(itemInList => <ListItem key={itemInList.id} text={itemInList.text} />)
}
```

The ListItem component is a dumb component that only knows to render an li element:

```html
const ListItem = props => <li key={props.id}>{props.text}</li>
```


